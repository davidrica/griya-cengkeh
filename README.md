#SSR preact async routes (Code split).
Steps to run the repo

- clone repo
- yarn install
development:
- yarn build
- yarn watch
- yarn serve
production:
- yarn start

#Skeleton project with latest packages
- Remove all packages inside dependencies and devDependencies
- yarn add body-parser cookie-parser debug express jade morgan preact-render-to-string serve-favicon undom
- yarn add -D @babel/core @babel/cli @babel/plugin-proposal-class-properties @babel/plugin-syntax-dynamic-import @babel/plugin-transform-react-jsx @babel/polyfill @babel/preset-es2015 @babel/preset-react @babel/preset-stage-0 babel-eslint babel-loader babel-plugin-dynamic-import-node-sync babel-plugin-syntax-async-functions babel-plugin-transform-node-env-inline babel-plugin-transform-react-remove-prop-types babel-preset-babili @babel/preset-env babel-regenerator-runtime chunk-manifest-webpack-plugin css-loader jsdom mini-css-extract-plugin mustache-express preact preact-async-route preact-material-components preact-router style-loader webpack webpack-cli webpack-node-externals window