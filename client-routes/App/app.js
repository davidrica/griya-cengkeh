import {Component, h} from 'preact';
import {Router} from 'preact-router';
import AsyncRoute from 'preact-async-route';
import TopAppBar from 'preact-material-components/TopAppBar';
import 'preact-material-components/TopAppBar/style.css';
import './app.css';

export default class App extends Component{
  getHome(url, cb, props){
    const componentOrPromise = import('../Home');
    if (componentOrPromise.then) {
      return componentOrPromise.then(module => module.default);
    } else if (componentOrPromise.default) {
      cb({component: componentOrPromise.default});
    }
  }

  getGallery(url, cb, props){
    const componentOrPromise = import('../Gallery');
    if (componentOrPromise.then) {
      return componentOrPromise.then(module => module.default);
    } else if (componentOrPromise.default) {
      cb({component: componentOrPromise.default});
    }
  }

  getContact(url, cb, props){
    const componentOrPromise = import('../Contact');
    if (componentOrPromise.then) {
      return componentOrPromise.then(module => module.default);
    } else if (componentOrPromise.default) {
      cb({component: componentOrPromise.default});
    }
  }

  getLoadingComponent(route) {
    if (this.state.ssrShown) {
      return <div>loading...</div>;
    } else {
      return <div dangerouslySetInnerHTML={{ __html: this.state.ssrText }} />;
    }
  }

  componentWillMount(){
    if(typeof document !== "undefined") {
      this.setState({
        ssrText: document.querySelector('.content').innerHTML
      });
    }
  }
  render(props) {
    return(
      <div>
        <TopAppBar className="topappbar">
            <TopAppBar.Row>
              <TopAppBar.Section align-start>
                <TopAppBar.Icon navigation>menu</TopAppBar.Icon>
                <TopAppBar.Title>
                  My App
                </TopAppBar.Title>
              </TopAppBar.Section>
              <TopAppBar.Section align-end>
                <TopAppBar.Icon>more_vert</TopAppBar.Icon>
              </TopAppBar.Section>
            </TopAppBar.Row>
          </TopAppBar>
        <div class="content">
          <Router {...props}>
            <AsyncRoute loading={this.getLoadingComponent.bind(this, 'Home')} path='/' getComponent={this.getHome}/>
            <AsyncRoute loading={this.getLoadingComponent.bind(this, 'Gallery')} path='/galeri' getComponent={this.getGallery} />
            <AsyncRoute loading={this.getLoadingComponent.bind(this, 'Contact')} path='/kontak-kami' getComponent={this.getContact} />
          </Router>
        </div>
      </div>
    )
  }
}