const webpack = require("webpack");
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const extractCSS = new MiniCssExtractPlugin('../css/[name].css');

const config = {
  entry: {
   client: './client-app.js',
   app: './client-routes/App/app.js'
  },
  output: {
    path: __dirname + '/public/',
    publicPath: '/public/',
    filename: '[name].js',
  },
  plugins: [new MiniCssExtractPlugin()],
  module: {
    rules: [
      {
        loader: 'babel-loader',
        test: /\.(js|jsx)$/,
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
	  ]
  }
};

module.exports = config;