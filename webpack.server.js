const webpack = require("webpack");
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const nodeExternals = require('webpack-node-externals');

// const extractCSS = new MiniCssExtractPlugin('../css/[name].css');
const config = {
  entry: {
   server: './app.js',
  },
  output: {
    path: path.join(__dirname, 'server'),
    filename: 'server.js',
  },
  target: 'node',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use:{
          loader: 'babel-loader',
          options: {
            plugins: ['dynamic-import-node-sync']
          }
        }
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
	  ]
  },
};

module.exports = config;